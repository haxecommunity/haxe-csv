== haxe-csv
simple cross platform csv library

Works:
- parsing csv

usage example:


parse from csv string:

  for (row in new StringReader(s, format));
  or
  using Lambda
  new StringReader(s, format).array()

parse from file:
  FileInputReader.fromFile('foo.csv', format).array();

TODO: optimize for PHP using fgetcsv etc
