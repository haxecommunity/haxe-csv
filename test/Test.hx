using Lambda;

class Test {

  static function main() {
    var testCases:Array<{csv:String, name:String, expected:Array<String>, format: CSVFormat}> = [
      {
        name: 'simple',
        csv: "abc,foo,bar",
        format: {
          sep: ","
        },
        expected: ["abc","foo","bar"]
      },
      {
        name: 'quoting  / escaping',
        csv: "abc;\"foo\";\"ba\\r\"",
        format: {
          quote: "\"",
          sep: ";",
          escape: "\\"
        },
        expected: ["abc","foo","bar"]
      },
      {
        name: 'empty cols',
        csv: ";;;",
        format: {
          quote: "\"",
          sep: ";",
          escape: "\\"
        },
        expected: ["","","",""]
      }
    ];

    for (t in testCases){
      var c = new csv.RowParser(t.format);
      var result = c.parseLine(t.csv);
      if (haxe.Serializer.run(result) != haxe.Serializer.run(t.expected))
        throw 'bad test case ${t.name} got: ${result}, expected ${t.expected}';
    }


    var testCases:Array<{csv:String, name:String, expected:Array<Array<String>>, format: CSVFormat}> = [
      {
        name: '2 lines',
        csv: "abc;\"foo\";\"ba\\r\"\n1;2;3",
        format: {
          quote: "\"",
          sep: ";",
          escape: "\\"
        },
        expected: [["abc","foo","bar"], ['1','2','3']]
      }
    ];

    for (t in testCases){
      var result = new csv.StringReader(t.csv, t.format).array();
      if (haxe.Serializer.run(result) != haxe.Serializer.run(t.expected))
        throw 'bad test case ${t.name} got: ${result} expected ${t.expected}';
    }

    var result = csv.FileInputReader.fromFile("demo.csv", {sep:';'}).array();
    var expected = [["abc","foo"],["bar","baz"]];
    if (haxe.Serializer.run(result) != haxe.Serializer.run(expected))
      throw "failed";
  }
}
