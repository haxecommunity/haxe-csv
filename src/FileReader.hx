
class FileReader {

  static public function iteratorFromString(file:String, format: CSVFormat = {}) {
  }

  static public function iteratorFromInputStream(i:InputStream, format: CSVFormat) {
    var x = new StringReader(format);
  }

  public function formatDefaults(f:CSVFormat) {
    f.del = f.del.ifNull(";");
    f.quote = f.quote.ifNull("\"");
    f.escape = f.escape.ifNull("\\");
    return f;
  }

  public function new(format){
  }

}
