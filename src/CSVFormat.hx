typedef CSVFormat = {
  // always only the first char will be taken into account (using fastCodeAt)
  ?sep:String, // col separator
  ?quote:String, // quoting, usually "
  ?escape:String, // escape, usually \
  // null means take \r or \n as line delemiter (in any order arbitrary often, thus this should fit \r\n)
  // this might change ..
  ?linedel:String
}
