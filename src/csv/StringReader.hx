package csv;
import csv.RowParser;
using Lambda;

class StringReader {

  public var fileInput:sys.io.FileInput;
  public var format: CSVFormat;
  public var sr: RowParser;
  public var current: Array<String>;

  public function new(s:String, format: CSVFormat, defaults = true) {
    this.format = format;
    if (defaults) RowParser.formatDefaults(this.format);
    sr = new RowParser(this.format);
    sr.csv = s;
  }

  public function hasNext(){
    if (sr.pos >= sr.l){ 
      current = null;
      return false;
    }
    current = sr.parseFields();
    sr.eatEol();
    return current != null;
  }

  public function next(){
    return current;
  }

  public function iterator():Iterator<Array<String>> {
    return this;
  }

}
