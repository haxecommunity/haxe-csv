package csv;
import csv.RowParser;
/* usage:
  for (cols in FileInputReader.fromFile("foo.csv", {del:","})){
  }
*/

class FileInputReader {

  public var fileInput:sys.io.FileInput;
  public var format: CSVFormat;
  public var sr: RowParser;
  public var current: Array<String>;

  static public function fromFile(file:String, format: CSVFormat, defaults = true) {
    return new FileInputReader(sys.io.File.read(file), format, defaults);
  }

  public function new(fileInput:sys.io.FileInput, format: CSVFormat, defaults = true) {
    this.format = format;
    if (defaults) RowParser.formatDefaults(this.format);
    this.fileInput = fileInput;
    sr = new RowParser(this.format);
  }

  static public function iteratorFromFileInput(i:sys.io.FileInput, format: CSVFormat) {
    var x = new RowParser(format);
  }

  public function hasNext(){
    try{
      if (fileInput.eof()){
        current = null;
        return false;
      }
      sr.csv = fileInput.readLine();
      if (sr.csv == null){
        current = null;
      } else {
        current = sr.parseFields();
      }
      return current != null;
    }catch(e:haxe.io.Eof){
      current = null;
      return false;
    }
  }

  public function next(){
    return current;
  }

  public function iterator():Iterator<Array<String>> {
    return this;
  }

}
