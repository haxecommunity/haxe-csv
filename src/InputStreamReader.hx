/* usage:
  for (cols in InputStreamReader.fromFile("foo.csv", {del:","})){
  }
*/

class InputStreamReader implements Iterator<Array<String>> {

  public var is:InputStream;
  public var format: CSVFormat;
  public var sr: StringReader;

  static public function fromFile(file:String, format: CSVFormat = {}) {
    return new InputStreamReader(new sys.io.File.read(file), format);
  }

  public function new(is:InputStream, format: CSVFormat, defaults = true) {
    this.format = format;
    if (defaults) StringReader.formatDefaults(this.format);
    this.is = is;
    sr = new StringReader(this.format);
  }

  public function defaults() {
    this.format = StringReader.default
  }

  static public function iteratorFromInputStream(i:InputStream, format: CSVFormat) {
    var x = new StringReader(format);
  }

  public function hasNext(){
    sr.csv = is.readLine();
    if (sr.csv == null){
      current = null;
    } else {
      current = sr.parseFields();
    }
    return current != null;
  }

  public function next(){
    return current;
  }

}
